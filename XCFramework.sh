#!/bin/sh

PRODUCT=FirebaseWrapper
BB="\033[1;34m"
NM="\033[0m"

echo "${BB}Removing products of previous build${NM}"
rm -rf Archives
rm -rf $PRODUCT.xcframework

echo "${BB}Building $PRODUCT for iOS${NM}"
xcodebuild clean archive -workspace $PRODUCT.xcworkspace -scheme $PRODUCT -sdk iphoneos -archivePath Archives/iOS BUILD_LIBRARY_FOR_DISTRIBUTION=YES SKIP_INSTALL=NO                    | xcpretty

echo "${BB}Building $PRODUCT for iOS Simulator${NM}"
xcodebuild clean archive -workspace $PRODUCT.xcworkspace -scheme $PRODUCT -sdk iphonesimulator -archivePath Archives/iOS-Simulator BUILD_LIBRARY_FOR_DISTRIBUTION=YES SKIP_INSTALL=NO   | xcpretty

echo "${BB}Generating XCFramework${NM}"
xcodebuild -create-xcframework                                                                  \
    -framework Archives/iOS.xcarchive/Products/Library/Frameworks/$PRODUCT.framework            \
    -framework Archives/iOS-Simulator.xcarchive/Products/Library/Frameworks/$PRODUCT.framework  \
    -output $PRODUCT.xcframework