//
//  ContentView.swift
//  Sample
//
//  Created by Patrick Lorran on 30/07/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentViewPreviews: PreviewProvider {
    
    static var previews: some View {
        ContentView()
    }
}
