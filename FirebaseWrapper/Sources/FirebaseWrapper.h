//
//  FirebaseWrapper.h
//  FirebaseWrapper
//
//  Created by Patrick Lorran on 30/07/21.
//

#import <Foundation/Foundation.h>

//! Project version number for FirebaseWrapper.
FOUNDATION_EXPORT double FirebaseWrapperVersionNumber;

//! Project version string for FirebaseWrapper.
FOUNDATION_EXPORT const unsigned char FirebaseWrapperVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FirebaseWrapper/PublicHeader.h>

NS_ASSUME_NONNULL_BEGIN

@interface FirebaseWrapper : NSObject

+ (void)configure;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
