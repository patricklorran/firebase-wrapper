//
//  FirebaseWrapper.m
//  FirebaseWrapper
//
//  Created by Patrick Lorran on 30/07/21.
//

#import "FirebaseWrapper.h"

#import <FirebaseAnalytics/FirebaseAnalytics.h>
#import <FirebaseCore/FirebaseCore.h>

@implementation FirebaseWrapper

+ (void)configure {
    [FIRApp configure];
}

@end
