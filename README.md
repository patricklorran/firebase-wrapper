# Firebase Wrapper

This project demonstrates how to statically link Firebase SDKs inside a dynamic framework.

## Requirements

You'll need Cocoapods and [xcpretty](https://github.com/xcpretty/xcpretty). You can install them by running the following commands:

```shell
$ gem install cocoapods
$ gem install xcpretty
```

## Initial setup

Run `$ pod install` to install all required dependencies. Then, navigate into the repository directory and execute the script to generate the XCFramework by running `$ sh XCFramework.sh`.

## Things to note

* The minimum deployment version supported by Firebase Analytics is iOS 11.0 and later. Assigning a lower version will make the build fail.
* Firebase Analytics does not build for macOS. So, enabling Mac Catalyst support will make the build fail when compiling for macOS.

## Author

[Patrick Lorran](https://patricklorran.com)